<?php

namespace ConnectMalves\JsonCrud\Models;

use ConnectMalves\JsonCrud\Models\BaseModel;

class Model extends BaseModel
{   
    /**
     * The json configuration filename.
     *
     * @var string
     */
    protected $json = "User.json";
}
