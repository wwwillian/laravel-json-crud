<?php

namespace ConnectMalves\JsonCrud\Repositories\Eloquent;

use ConnectMalves\JsonCrud\Repositories\Eloquent\BaseRepository;

class Repository extends BaseRepository
{
    protected $modelClass = ConnectMalves\JsonCrud\Models\Model::class;
}
