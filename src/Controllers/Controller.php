<?php

namespace ConnectMalves\JsonCrud\Controllers;

use ConnectMalves\JsonCrud\Controllers\BaseApiController;

class Controller extends BaseApiController
{
    protected $serviceClass = ConnectMalves\JsonCrud\Services\Service::class;
}
